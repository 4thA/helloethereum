'use strict';

const _ = require('lodash');
const assert = require('assert');
const truffle_contract = require("truffle-contract");
const express = require('express');
const fs = require('fs');
const Web3_truffle = require('./node_modules/truffle-contract/node_modules/web3');
const Web3_latest = require('web3');

// Note: truffle-contract includes a version of web3, but not the one we want to use.
// We want to use web3 v1.0 (beta), so we can use web3's new 'Contract' class.

const app = express();
const port = 8080;

const network_id_foundation = 1;
const network_id_kovan      = 42;

function getEthereumRpcAddress()
{
	// Check command line arguments for "truffle"
	// 
	// process.argv[0]: "/path/to/node"
	// process.argv[1]: "/path/to/gdaxAutoBuyer.js"
	// process.argv[2]: "truffle" (optional)
	
	if ((process.argv.length > 2) && (process.argv[2] == "truffle"))
	{
		return "http://localhost:9545"; // truffle
	}
	else
	{
		return "http://localhost:8545"; // parity, testrpc
	}
}

function getHelloEthereumTruffleContract()
{
	let he_str = fs.readFileSync("./contract/build/contracts/HelloEthereum.json", "utf8");
	let he_obj = JSON.parse(he_str);
	
	let he_contract = truffle_contract(he_obj);
	
	// The truffle-contract expects a 'provider' that's compatible with it's own
	// internal version of web3. Unfortunately, this doesn't always match the version we
	// want to use. So we need to explicitly use Web3_tuffle here.
	
	let provider = new Web3_truffle.providers.HttpProvider(getEthereumRpcAddress());
	he_contract.setProvider(provider);
	
	return he_contract;
}

function getNetworkID(deployed)
{
	let matching_network_id = null;
	
	let networks = null;
	if (deployed.networks) {
		networks = deployed.networks;
	}
	else if (deployed.constructor) {
		networks = deployed.constructor.networks;
	}
	
	if (_.isObject(networks))
	{
		let deployed_address = deployed.address;
		
		for (let network_id in networks)
		{
			let network_info = networks[network_id];
			let network_address = network_info.address;
			
			if (network_address == deployed_address)
			{
				matching_network_id = network_id;
				break;
			}
		}
	}
	
	return matching_network_id;
}

function getTransactionOptions(deployed)
{
	assert(_.isObject(deployed), "Bad parameter: deployed");

	// Our contract is structured such that only the account that
	// deployed the contract (the owner) is allowed to execute transactions.
	//
	// So we want to use that same account.

//	console.log("deployed: "+ JSON.stringify(deployed, null, 2));

	let account = null;
	if (deployed.contract && deployed.contract._eth)
	{
		// Sometimes the truffle-contract instance has the information we need.
		// Although I'm not entirely sure how it gets this information...

		let coinbase = deployed.contract._eth.coinbase;
		let accounts = deployed.contract._eth.accounts;

		if (coinbase != null)
		{
			// We know the owner of the contract.
			// So let's use that.
			account = coinbase;
		}
		else if (_.isArray(accounts) && accounts.length > 0)
		{
			// Fallback to using one of the accounts in the list.
			account = accounts[0];
		}
	}

	if (account)
	{
		let options = { from: account };
		return options;
	}

	// Otherwise we need to fall back to manually setting our account info.
	// This is hard-coded here.
	//
	// But probably the proper way to do this would be to query the parity server.
	// That is, we'd call 'personal_listAccounts' to get the available accounts.
	// And then we'd enumerate them, calling 'eth_getBalance' to find one with money.
	//
	// However, I will leave this as an excercise for you. :)

	let options = {};
	
	let network_id = getNetworkID(deployed);
	if (network_id)
	{
		switch (network_id)
		{
			case network_id_foundation : // Foundation (mainnet)
			{ 
			//	options = { from: "0x.....?" };
				break;
			}
			case network_id_kovan: // Kovan (testnet)
			{
			//	options = { from: "0x.....?" };
				break;
			}
		}
	}
	
	return options;
}

function getContract(deployed)
{
	let options = getTransactionOptions(deployed);
	console.log("options: "+ JSON.stringify(options, null, 2));
	
	let provider = new Web3_latest.providers.HttpProvider(getEthereumRpcAddress());
	let web3 = new Web3_latest(provider);
	
	let contract = new web3.eth.Contract(deployed.abi, deployed.address, options);
	
	return contract;
}

function get_value_for_key(req, res)
{
	console.log("GET "+ req.originalUrl);
	
	const key = req.query.key;
	if (!key || key.length === 0)
	{
		res.status(400).json({ message: "Missing/invalid query parameter: key" });
		return;
	}
	
	console.log("key: "+ key);
	
	let helloEthereum = getHelloEthereumTruffleContract();
	let contract = null;
	
	helloEthereum.deployed().then(function(deployed) {
		
		console.log("HelloEthereum contract address: "+ deployed.address);
		contract = getContract(deployed);
		
		// Next step
		_get_value_for_key();
		
	}, function(err){
		
		console.log("helloEthereum.deployed(): err "+ err);
		console.log(err.stack || err);
		
		res.status(500).json({
			"message": "Internal server error",
			"error": "Ethereum contract does not appear to be deployed"
		});
		return;
	});
	
	function _get_value_for_key()
	{
		console.log("contract.getValue()...");
		contract.methods.getValue(key).call({}, function(err, value){
			
			if (err)
			{
				console.log("contract.getValue(): err: "+ err);
				
				res.status(500).json({ "error": "getValue() failed" });
				return;
			}
			
			console.log("contract.getValue(): "+ value);
			
			res.status(200).json({
				key,
				"value": (value.length === 0 ? null : value)
			});
		});
	}
}

function set_value_for_key(req, res)
{
	console.log("POST "+ req.originalUrl);
	
	const key = req.query.key;
	if (!key || key.length === 0)
	{
		res.status(400).json({ message: "Missing/invalid query parameter: key" });
		return;
	}
	
	const value = req.query.value;
	if (!value || value.length === 0)
	{
		res.status(400).json({ message: "Missing/invalid query parameter: value" });
		return;
	}
	
	console.log("key: "+ key);
	console.log("value: "+ value);
	
	let helloEthereum = getHelloEthereumTruffleContract();
	let contract = null;
	
	helloEthereum.deployed().then(function(deployed) {
		
		console.log("HelloEthereum contract address: "+ deployed.address);
		contract = getContract(deployed);
		
		// Next step
		_set_value_for_key();
		
	}, function(err){
		
		console.log("helloEthereum.deployed(): err "+ err);
		console.log(err.stack || err);
		
		res.status(500).json({
			"message": "Internal server error",
			"error": "Ethereum contract does not appear to be deployed"
		});
		return;
	});
	
	function _set_value_for_key()
	{
		console.log("contract.setValue()...");
		contract.methods.setValue(key, value).send({}).then(function(receipt){
			
			console.log("contract.addPubKey(): receipt: "+ JSON.stringify(receipt, null, 2));
			
			let wasAdded = false;
			if (receipt.events && receipt.events.ValueAdded)
			{
				wasAdded = true;
			}
			
			res.status(200).json({
				"result": wasAdded,
				key, value
			});
			
		}, function(err){
			
			console.log("contract.addPubKey(): err: "+ err);
			
			res.status(500).json({
				"error": JSON.stringify(err)
			});
		});
	}
}

app.get('/helloEthereum', get_value_for_key);
app.post('/helloEthereum', set_value_for_key);

app.listen(port, function () {
	
	console.log('Server listening on port '+ port);
});
