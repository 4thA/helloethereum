pragma solidity ^0.4.15;

contract HelloEthereum {
	address owner;

	mapping(string => string) values;

	event ValueAdded(string key);

	function HelloEthereum() public {
		owner = msg.sender;
	}

	modifier onlyByOwner()
	{
		if (msg.sender != owner)
			require(false);
		else
			_;
	}

	function setValue(string key, string value) public {

		if (bytes(key).length == 0) require(false);   // key must not be empty string
		if (bytes(value).length == 0) require(false); // value must not be empty string

		string storage existingValue = values[key];
		if (bytes(existingValue).length == 0) {
			values[key] = value;
			ValueAdded(key);
		}
	}

	function getValue(string key) public constant returns (string) {

		string storage value = values[key];
		return value;
	}

	function kill() onlyByOwner public {
		selfdestruct(owner);
	}
}
