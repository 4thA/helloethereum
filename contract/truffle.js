module.exports = {
	networks: {
		development: {
			host: "localhost",
			port: 8545,
			network_id: "42", // Kovan (testnet)
		},
		production: {
			host: "localhost",
			port: 8545,
			network_id: "1" // Foundation (mainnet)
 		}
  }
};
